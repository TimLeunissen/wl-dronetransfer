let appRootPath 	= require('app-root-path')


module.exports = {
	app: {
		root: `${appRootPath}`,
	},

//	 upload: {
//	 	server: 'https://woodl.nl',
//	 	method: 'POST',
//	 	endpoint: '/images/upload/',
//	 	source: '/images/',
//	 },

	upload: {
		server: 'http://agrifly5g.housing.rug.nl:3443',
		method: 'POST',
		endpoint: '/images/upload/',
		source: '/images/',
	},
}
