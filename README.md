# WL - Dronetransfer

## How to use

0. Get a clean installation of Raspberry
```bash
# Install node & npm
# <find commands for this on Google>

# Global n & pm2
sudo npm install -g n pm2@2

# Add startup requirements for PM2
pm2 startup
# - Follow instructions given!

# Install LTS version of node & npm
n lts 
```

1. Clone folder on Raspberry Pi and install packages

```bash

# Clone repository
git clone #<add repo>

# Change directory into repo
cd wl-dronetransfer

# Install required packages
npm install
```

2. Setup repositoruy to run as 

```bash
pm2 start ~/wl-dronetransfer/app.js
pm2 save
```

