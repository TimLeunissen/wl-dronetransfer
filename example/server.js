let express = require('express')
let app = express()
let fs = require('fs')
let path = require('path')

app.get('/', (req, res) => res.send('Hello World!'))

// Lists all files in specified folder
//
app.get('/images/list', (req, res) => {

	let grouped = {}

	let files = fs
		.readdirSync(config.app.root + '/images-public/')
		.filter(name => name.match(/IMG_[0-9]{6}_[0-9]{6}_[0-9]{4}/))
		.filter(name => name.endsWith('.JPG'))
		.map(name => ({
			name: name.slice(0, -7)
		}))

	// Send all files as json
	res.send({
		data: {
			files: files
		}
	})
})

// Upload new file
//
app.post('/images/upload/:filename', (req, res) => {

	// Get filename
	let filename = req.params.filename;

	// Write file into folder
	req.pipe(fs.createWriteStream(config.app.root + '/images-public/' + filename))
		.on('data', (data) => console.log(data))
		.on('finish', () => {

			// Let client know that image has arrived correctly
			res.send({
				data: {
					transfer: 'perfect',
				}
			})
		})
})

app.use(express.static(path.join(config.app.root, 'images-public')))

app.listen(3000, () => console.log(`
Welcome to your first Node.JS server!
	
(1) Check localhost:3000 to see if working
(2) Check localhost:3000/example.jpeg to see if public folder works
(3) Check localhost:3000/images/list to see if listing files works (should show no files since none match the specified format)
(4) Completed step (1) (2) (3) get coffee!
`))