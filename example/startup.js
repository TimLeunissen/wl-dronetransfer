
// On startup start logging Hello World every 15 seconds
var helloWorld = () => console.log('Hello World @ ' + Date.now())
helloWorld()

setInterval(helloWorld, 15 * 1000)