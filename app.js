

// This file shows a single command to the sequoia from the raspberry,
// it downloads a single (currently) predifined file. 
// In later versions, a web api could be created (on the raspberry) to control file transfers


// 1. Get pictures from connected Parrot Sequoia 
// 2. Transfer pictures to a defined server
// 3. Forward commands sent from server

let fs 								= require('fs')

// Global configuration
config	 							= require('./app.config')

// Start transfer
let transferToServer 				= require('./transfer-to-server')
let transferFromSequoiaStartPyhon 	= require('./transfer-from-sequoia')
