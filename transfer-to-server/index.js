let request = require('request')
let fs = require('fs')
let debug = require('debug')('transfer-to-server')
let config = require('../app.config')

// 1. Read all files in folder
// 2. Start transfer
// 3. Remove file from disk

init()

function init() {

	debug('We started')
	ListFiles({

		files: [],

		// The actual file
		file: {
			name: undefined,
			stream: undefined,
		},

		// The connection with server
		server: {
			request: undefined,
			ip: config.upload.server,
			endpoint: config.upload.endpoint,
		}
	})
	.then(ReadFile)
	.then(TransferFile)
	.then(RemoveFile)

	// Handling
	.then(HandleSuccess)
	.catch(HandleError)

}

function ListFiles(data) {
	return new Promise((resolve, reject) => {

		// Get all files
		data.files = fs.readdirSync(config.app.root + config.upload.source)

			// Filter out correct ones
			// .filter(name => name.match(/IMG_[0-9]{6}_[0-9]{6}_[0-9]{4}/))
			//.filter(name => name.match(/.TIF/))

			// Add full path
			.map(name => ({
				name: name,
				path: config.app.root + config.upload.source + name
			})
			)

		// Add full path
		debug(`Total of ${data.files.length} files was found!`)

		// Check if no files to transfer
		if (data.files.length === 0) return reject({
			message: 'No more files to read',
			timeout: 1000,
		})

		resolve(data)
	})
}

function ReadFile(data) {
	return new Promise((resolve, reject) => {

		// Create read stream
		data.file = data.files[0]
		data.file.stream = fs.createReadStream(data.file.path)

		// Continue
		resolve(data)
	})
}

function TransferFile(data) {
	return new Promise((resolve, reject) => {

		// Server response handler
		const transferFileStart = Date.now()
		const method = config.upload.method
		const uri = `${data.server.ip}${data.server.endpoint}${transferFileStart}-${data.file.name}`;

		debug(uri)

		const response = (err, _, resp) => {

			// Clear response
			data.file.stream.close()

			// Check error
			if (err) return reject({
				message: 'File transfer failed!',
				error: err,
			})
	
			console.log(`Transfer to server result:`)
			console.log(` - Method:   ${method}`)
			console.log(` - Duration: ${Date.now() - transferFileStart} ms`)
			console.log(` - Filename: ${uri}`)

			// Parse response
			try {
				resp = JSON.parse(resp)
			}
			catch (err) {
				return reject({
					message: 'Invalid JSON',
					error: err,
				})
			}

			// Check if response is perfect
			if (resp && resp.data && resp.data.transfer === 'perfect') return resolve(data)

			// Other fail
			reject({
				message: 'Unknown fail',
				data: resp,
				err: err,
			})
		}
		
		// Start transfer
		data.file.stream.pipe(request({ method, uri, gzip: true }, response))

	})
}

function RemoveFile(data) {
	return new Promise((resolve, reject) => {

		// If you want to keep the same
		// return resolve(data)

		fs.unlink(data.file.path, err => {

			if (err) return reject({
				message: 'Failed while removing file',
				error: err,
			})

			resolve(data)
		})
	})
}

function HandleSuccess() {
	init()
}

function HandleError(err) {

	// Check for timeout
	if (err && err.timeout) {
		debug(err.message)
		return setTimeout(init, err.timeout)
	}

	console.log('We encountered an error')
	console.log(err)
	init()
}