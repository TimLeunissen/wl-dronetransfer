import os
import sys
import time
from ptpy import PTPy
from ptpy.transports.usb import find_usb_cameras

def main():
    while True:
        arguments = sys.stdin.readline()
        arguments = arguments.split(':')

        global busy
        global camera
        
        if busy:
            return False
        else:
            # print('Setting busy -> True')
            busy = True

            # Start doing stuff
            with camera.session():
                handles = camera.get_object_handles(
                    0,
                    all_storage_ids=True,
                    all_formats=True,
                )
                for handle in handles:
                    info = camera.get_object_info(handle)
                    # print(info)

                    # Download all things that are not groups of other things.
                    # time.sleep(5)
                    if info.ObjectFormat == 'TIFF' or info.ObjectFormat == 'EXIF_JPEG':
                        # print("Obtaining image from camera")
                        obj = camera.get_object(handle)
                        # print(info.StorageID)
                        # print(info.ObjectFormat)
                        with open(os.path.join(image_output_folder, info.Filename), mode='w') as f:
                            f.write(obj.Data)
                        # print("Deleting image from camera", info.Filename)
                        camera.delete_object(handle)
                        # print('---------------------------------')

            # print('Setting busy -> False')
            busy = False

        sys.stdout.write("We did what we had to do, please trigger me again!\n")
        sys.stdout.flush()

if __name__ == '__main__':

    busy = False
    image_output_folder = 'images'
    obtained_image_storage_ids = []

    print('Connecting to camera')
    for i, device in enumerate(find_usb_cameras()):
        print('A device found!')
        try:
            device.reset()
            camera = PTPy(device=device)
            print('Connected.')
        except Exception as e:
            print('Please try to reconnect the camera')
            print(e)
    
    if not os.path.isdir(image_output_folder):
        os.makedirs(image_output_folder)

    print('Start main.')
    main()