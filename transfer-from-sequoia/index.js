const PythonShell = require('python-shell').PythonShell;

const pyShellFile = `index.py`
const pyShellOptions = {
	scriptPath: `${config.app.root}/transfer-from-sequoia/`,
	mode: 'text', // [json text binary]
	pythonPath: '/usr/bin/python'
}

const pyShellCallback = (err, res) => {
	console.log('Script finished')
	console.log(results)
	console.log(err)
}

let pyShell;
let pyShellCommandInterval = 30 * 1000;
let pyShellCommandLast = Date.now()

const init = () => {

	pyShell = new PythonShell(pyShellFile, pyShellOptions, pyShellCallback) 

	pyShell.on('error', (err) => {
		console.log('we got an error')
		console.log(err)

		// Restart
		init()
	})

	// Start
	pyShellCommandInit()

	// Whenever we have a response, start sending next request
	pyShell.on('message', (message) => {
		console.log(message);
		pyShellCommandInit()
	})
}

const pyShellCommandInit = () => {
	const timeAfterLast = Date.now() - pyShellCommandLast
	const wait = Math.max(pyShellCommandInterval - timeAfterLast, 0)

	setTimeout(() => {
		if (!pyShell) return console.log('NOTE: pyshell does not exist')

		pyShellCommandLast = Date.now()
		const pyShellCommandNext = 'download_and_delete_all'
		pyShell.send(pyShellCommandNext)
	}, wait)
}

init()

process.on('SIGINT', () => {
	console.log('Process')
	pyShell.on('end', (message) => {
		console.log('corectly ended')
	})
})